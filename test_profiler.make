core = 7.x
api = 2

projects[drupal][type] = "core"
projects[drupal][download][type] = "get"
projects[drupal][download][url] = "http://ftp.drupal.org/files/projects/drupal-7.33.tar.gz" 

; Contrib
projects[admin_menu] = "3.0-rc4"
projects[views] = "3.8"
projects[features] = "2.2"
projects[pathauto] = "1.2"
projects[ctools] = "1.4"

libraries[profiler][download][type] = "get"
libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-7.x-2.0-beta2.tar.gz"

; profile
projects[mig5_net][type] = "profile"
projects[mig5_net][download][type] = "git"
projects[mig5_net][download][url] = "git@bitbucket.org:audrius112/profile1.git"
projects[mig5_net][download][branch] = "master"
